import Vue from 'vue'
import Router from 'vue-router'
import Cards from '@/components/Cards'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: require('../components/Home.vue').default,
      name: 'home'
    }, {
      path:'/cards',
      name: 'cards',
      // component: require('../components/Cards.vue')
      component: Cards
    }, {
      path: '/a',
      name: 'a-alone',
      component: require('../components/PageA.vue').default,
      children: [{
        path: 'b',
        name: 'a.b',
        component: require('../components/PageB.vue').default
      }, {
        path: 'c',
        name: 'a.c',
        component: require('../components/PageC.vue').default
      }]
    }, {
      path: '/a/:id(\\d+)',
      name: 'a',
      component: require('../components/PageA.vue').default
    }
  ]
})
